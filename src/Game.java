import java.util.Scanner;
/*
*Главный клас позволяющий потопить корабль и подсчитать за сколько выстрелов мы это сделали
*
*
*
 */
public class Game {
    public static final String ENTER_THE_SIZE = "Введите размер канала";
    public static final String ERROR_IN_THE_WATER_CHANNEL = "Вы ввели размер канала, куда не помещается корабль, попробуйте ввести размер заного";
    public static final String COORDINATE_INPUT = "Введите координату места куда будете стрелять";
    public static final String INPUT_ERROR_NUMBER = "Пробуйте ввести число которое, будет больше 0 и меньше длинны канала ";
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){
        System.out.println( ENTER_THE_SIZE);
        int channel = scanner.nextInt();
        while(channel < 2 ){
            System.out.println(ERROR_IN_THE_WATER_CHANNEL);
            channel = scanner.nextInt();
        }
        Ship ship = new Ship (channel);
        channel++;
        System.out.println("Вы потопили корабль за " + countAndShooting(channel) + " попыток." + " Спасибо за игру");

    }
    /*
    *Метод счетающий количество выстрелов пользователя и реализующий саму стрельбу
    *проверяет корректность ввода координат
    * @return количество выстрелов
     */
    public static int countAndShooting (int channel) {
        boolean result;
        int count = 0;
        do {
            System.out.println(COORDINATE_INPUT);
            int shot = scanner.nextInt();
            while(shot < 0 || shot >= channel){
                System.out.println(INPUT_ERROR_NUMBER);
                shot = scanner.nextInt();
            }
            result = Ship.isDestroyed(shot);
            count++;
        }
        while (result != true);
        return count;
    }

}

