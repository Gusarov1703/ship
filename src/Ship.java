import java.util.ArrayList;
/**
 *класс описывающий корабль
 *
 *@author Gusarov Evgeniy
 */
public class Ship{
    public static final String THEY_ALREADY_SHOT_HERE = "в это место уже стреляли попробуйте ещё раз";
    public static final String WOUND = "Вы попали по кораблю!";
    public static final String FAILURE = "Промахнулись, но не растраивайтесь попробуйте ещё раз";
    public static final String DESTROYED = "Корабль потоплен";
    //координаты корабля
    private static ArrayList coordinateShip = new ArrayList();
    //коодината выстрела
    private static ArrayList coordinateShot = new ArrayList();

    /**
     * конструктор с аргументам, задающий рандомно местоположение коробля на канале
     * @param channel
     */
    public Ship (int channel){
        int a = (int) (Math.random()*channel);
        coordinateShip.add(a);
        coordinateShip.add(a+1);
        coordinateShip.add(a+2);

    }

    /**
     * конструктор по уполчанию, размещающий корабль в начале канала
     */
    public Ship (){
        coordinateShip.add(0);
        coordinateShip.add(1);
        coordinateShip.add(2);
    }

    /**
     *Метод, проверяющий, попал ли пользователь в корабль
     * @param shot координата выстрела
     * @return значение true(потопил) или false(не потопил)
     */

    public static boolean isDestroyed(int shot) {
        if(coordinateShot.contains(shot)){
            System.out.println(THEY_ALREADY_SHOT_HERE);
        }else{
            if (!coordinateShip.contains(shot)){
                System.out.println(FAILURE);
                coordinateShot.add(shot);
            }else{
                int x = coordinateShip.indexOf(shot);
                coordinateShip.remove(x);
                coordinateShot.add(shot);
                System.out.println(WOUND);
            }
        }
        if (coordinateShip.isEmpty()) {
            System.out.println(DESTROYED);
        }
        return coordinateShip.isEmpty();
    }
}

